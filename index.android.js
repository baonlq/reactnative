import React, { Component } from 'react';
var ReactNative = require('react-native');
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Alert,
    Image,
    ScrollView,
    ListView,
    RecyclerViewBackedScrollView
} from 'react-native';

var NewsItems = require('./components/news-items');
var WebPage = require('./components/webpage');

var ROUTES = {
    news_items: NewsItems,
    web_page: WebPage
};


var AwesomeProject = React.createClass({

    renderScene: function(route, navigator) {

        var Component = ROUTES[route.name];
        return (
            <Component route={route} navigator={navigator} url={route.url} />
        );
    },

    render: function() {
        return (
            <Navigator
                style={styles.container}
                initialRoute={{name: 'news_items', url: ''}}
                renderScene={this.renderScene}
                configureScene={() => { return Navigator.SceneConfigs.FloatFromRight; }} />
        );

    },


});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    toolbar: {
        backgroundColor: '#673AB7',
        height: 56,
    }
});
AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
