import React, { Component } from 'react';
import { AppRegistry, Image } from 'react-native';

class ProfileView extends Component {
    render() {
        let pic = {
            uri: 'http://shushi168.com/data/out/92/36854874-girl-pictures.jpg'
        };
        return (
            <View>
                <Image source={pic} style={{width: 193, height: 110}}/>
            </View>
        );
    }
}

AppRegistry.registerComponent('HomeView', () => ProfileView);

module.exports = ProfileView;
