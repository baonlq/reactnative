import React, { Component } from 'react';
import { AppRegistry, Image } from 'react-native';

class HomeView extends Component {
    render() {
        let pic = {
            uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
        };
        return (
            <View>
                <Image source={pic} style={{width: 193, height: 110}}/>
            </View>
        );
    }
}

AppRegistry.registerComponent('HomeView', () => HomeView);

module.exports = HomeView;
